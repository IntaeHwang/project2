/*
	Project 2
	Name of Project: Hunter Assassin Map Editor
	Author: Intae Hwang
	Date: 6/18
*/
var mapEditor =
{
	obstacles: [],
	rows: 15,
	cols: 15
}

let blockSize = 900/(mapEditor.rows+1);
let brushType = "Wood";
let woodNumber = 0;
let guardNumber = 0;
let complexity = 0;

function setup (){
  createCanvas(900, 1080);
  for(let i=0; i<mapEditor.rows+2; i++)
  {
	  mapEditor.obstacles[i]=[];
  }
}

function draw(){
  background(120);
  //image (img, 0, 0, img.width, img.height);
  for(let i=0; i<=mapEditor.rows; i++){
	stroke(255);
	strokeWeight(1);
	line(450/(mapEditor.rows+1),(900*i+450)/(mapEditor.rows+1),900-450/(mapEditor.rows+1),(900*i+450)/(mapEditor.rows+1));
  }
  for(let i=0; i<=mapEditor.cols; i++){
	stroke(255);
	strokeWeight(1);
	line((900*i+450)/(mapEditor.cols+1),450/(mapEditor.cols+1),(900*i+450)/(mapEditor.cols+1),900-450/(mapEditor.cols+1));
  }
  for(let i=0; i<mapEditor.rows+2; i++)
  {
	  for(let j=0; j<mapEditor.cols+2; j++)
	  {
		if(i==0 || j==0 || i==mapEditor.rows+1 || j==mapEditor.cols+1 || mapEditor.obstacles[i][j]==true)
		{
		  	image(wood, blockSize*i-450/(mapEditor.rows+1), blockSize*j-450/(mapEditor.rows+1), blockSize, blockSize);
		}
		else if(mapEditor.obstacles[i][j]==false)
		{
			image(guard, blockSize*i-450/(mapEditor.rows+1), blockSize*j-450/(mapEditor.rows+1), blockSize*assassin.width/assassin.height, blockSize);
		}
	  }
  }
  image(assassin, 450-blockSize*assassin.width/assassin.height/2, 900-3*blockSize/2,blockSize*assassin.width/assassin.height,blockSize);
  image(go,360,900,180,180);
  image(up,540,900,90,90);
  image(down,540,990,90,90);
  image(woodDraw,630,900,90,90);
  image(guardDrawGUI,720,900,90,90);
  image(eraser,810,900,90,90);
  image(clearGUI,630,990,270,90);
  image(display,0,900,360,180)

  textSize(20);
  fill(255);
  noStroke();
  countStats();
  text('Welcome to the Map Customizer',30,950);
  text('MAP_SIZE : '+mapEditor.rows,30,980);
  text('MAP_COMPLEXITY : '+complexity,30,1010);
  text('GUARD_NUMBER : '+guardNumber,30,1040);

  if(brushType=="Wood") image(frame,630,900,90,90);
  if(brushType=="Guard") image(frame,720,900,90,90);
  if(brushType.includes("Eraser")) image(frame,810,900,90,90);

  if(450/(mapEditor.rows+1)<mouseX && mouseX<900-450/(mapEditor.rows+1) && 450/(mapEditor.rows+1)<mouseY && mouseY<900-450/(mapEditor.rows+1))
	{
		let x = Math.floor((mouseX+450/(mapEditor.rows+1))/ blockSize);
		let y = Math.floor((mouseY+450/(mapEditor.rows+1))/ blockSize);
		if(brushType=="Wood")
		{
			image(woodMouse, (x-0.5)*blockSize, (y-0.5)*blockSize, blockSize, blockSize);
		}
		if(brushType=="Guard")
		{
			image(guardMouse, (x-0.5)*blockSize, (y-0.5)*blockSize, blockSize*assassin.width/assassin.height, blockSize);
		}
		if(brushType.includes("Eraser"))
		{
			image(eraserMouse, (x-0.5)*blockSize, (y-0.5)*blockSize, blockSize*eraserMouse.width/eraserMouse.height, blockSize);
		}
	}

}

function mousePressed() {
	//Clicked on map
	if(450/(mapEditor.rows+1)<mouseX && mouseX<900-450/(mapEditor.rows+1) && 450/(mapEditor.rows+1)<mouseY && mouseY<900-450/(mapEditor.rows+1))
	{
    	let x = Math.floor((mouseX+450/(mapEditor.rows+1))/ blockSize);
    	let y = Math.floor((mouseY+450/(mapEditor.rows+1))/ blockSize);
		if(mapEditor.obstacles[x][y]==undefined)
		{
			if(!(Math.abs((mapEditor.rows+1)/2-x)<=0.5 && y==mapEditor.cols))
			{
				if(brushType=="Wood") mapEditor.obstacles[x][y] = true;
				else if(brushType=="Guard") mapEditor.obstacles[x][y] = false;
			}
		}
		else if(mapEditor.obstacles[x][y]!=undefined)
		{
			if(brushType=="Wood")
			{
				mapEditor.obstacles[x][y] = undefined;
				brushType = "WoodEraser";
			}
			else if(brushType=="Guard")
			{
				mapEditor.obstacles[x][y] = undefined;
				brushType = "GuardEraser";
			}
			else if(brushType=="Eraser")
			{
				mapEditor.obstacles[x][y] = undefined;
			}
		}
	}
	  //Clicked Go
	  if(360<mouseX&&mouseX<540&&900<mouseY&&mouseY<1080)
	  {
		let send =
		{
			grid: [],
			size: 0,
			complexity: 0,
			guardNumber: 0
		}
		let woodNumber = 0;
		for(let i=0; i<mapEditor.rows+2; i++)
  		{
	  		for(let j=0; j<mapEditor.cols+2; j++)
	  		{
				
				if(i==0 || j==0 || i==mapEditor.rows+1 || j==mapEditor.cols+1 || mapEditor.obstacles[i][j]==true)
				{
					let obj = {
						type: "Wood",
						i: i,
						j: j
					}
					send.grid.push(obj);
					woodNumber++;
				}
				if(mapEditor.obstacles[i][j]==false)
				{
					let obj = {
						type: "Guard",
						i: i,
						j: j
					}
					send.grid.push(obj);
					send.guardNumber++;
				}
			}
		}
		send.size = mapEditor.rows;
		send.complexity = Math.round(100*(woodNumber-4*mapEditor.rows-4)/mapEditor.rows**2);
		ws.send(JSON.stringify(send));
	  }
	  //Clicked Up
	  if(540<mouseX&&mouseX<630&&900<mouseY&&mouseY<990)
	  {
		if(mapEditor.rows<30)
		{
			mapEditor.rows++;
			mapEditor.cols++;
			blockSize = 900/(mapEditor.rows+1);
			mapEditor.obstacles = [];
			for(let i=0; i<mapEditor.rows+2; i++)
  			{
	  			mapEditor.obstacles[i]=[];
  			}
		}
	  }
	  //Clicked Down
	  if(540<mouseX&&mouseX<630&&990<mouseY&&mouseY<1080)
	  {
		if(mapEditor.rows>6)
		{
			mapEditor.rows--;
			mapEditor.cols--;
			blockSize = 900/(mapEditor.rows+1);
			mapEditor.obstacles = [];
			for(let i=0; i<mapEditor.rows+2; i++)
  			{
	  			mapEditor.obstacles[i]=[];
  			}
		}
	  }
	  //Clicked Wood
	  if(630<mouseX&&mouseX<720&&900<mouseY&&mouseY<990)
	  {
		brushType="Wood";
	  }
	  //Clicked Guard
	  if(720<mouseX&&mouseX<810&&900<mouseY&&mouseY<990)
	  {
		brushType="Guard";
	  }
	  //Clicked Eraser
	  if(810<mouseX&&mouseX<900&&900<mouseY&&mouseY<990)
	  {
		brushType="Eraser"
	  }
	  //Clicked Clear
	  if(630<mouseX&&mouseX<900&&990<mouseY&&mouseY<1080)
	  {
		mapEditor.obstacles = [];
		for(let i=0; i<mapEditor.rows+2; i++)
  		{
	  		mapEditor.obstacles[i]=[];
  		}
	  }
}

function mouseDragged()
{
	if(450/(mapEditor.rows+1)<mouseX && mouseX<900-450/(mapEditor.rows+1) && 450/(mapEditor.rows+1)<mouseY && mouseY<900-450/(mapEditor.rows+1))
	{
		let x = Math.floor((mouseX+450/(mapEditor.rows+1))/ blockSize);
		let y = Math.floor((mouseY+450/(mapEditor.rows+1))/ blockSize);
		if(brushType=="Wood" && mapEditor.obstacles[x][y]==undefined)
		{
			if(!(Math.abs((mapEditor.rows+1)/2-x)<=0.5 && y==mapEditor.cols))
			{
				mapEditor.obstacles[x][y] = true;
			}
		}
		else if(brushType.includes("Eraser") && mapEditor.obstacles[x][y]!=undefined)
		{
			mapEditor.obstacles[x][y] = undefined;
		}
	}
}

function mouseReleased()
{
	if(brushType.includes("Wood")) brushType = "Wood";
	if(brushType.includes("Guard")) brushType = "Guard";
}

function preload() {
	wood = loadImage('assets/Wood.png');
	assassin = loadImage('assets/assassin.png');
	guard = loadImage('assets/guard.png');
	
	go = loadImage('assets/Go.png');
	up = loadImage('assets/Up.png');
	down = loadImage('assets/Down.png');
	woodDraw = loadImage('assets/WoodDraw.png');
	guardDrawGUI = loadImage('assets/GuardDraw.png');
	eraser = loadImage('assets/Eraser.png');
	clearGUI = loadImage('assets/Clear.png');
	frame = loadImage('assets/Frame.png');
	woodMouse = loadImage('assets/WoodMouse.png');
	guardMouse = loadImage('assets/Guardmouse.png');
	eraserMouse = loadImage('assets/EraserMouse.png');
	display = loadImage('assets/display.png');
}

function countStats()
{
	woodNumber = 0;
	guardNumber = 0;
	complexity = 0;
	for(let i=0; i<mapEditor.rows+2; i++)
  	{
	  	for(let j=0; j<mapEditor.cols+2; j++)
	  	{	
			if(i==0 || j==0 || i==mapEditor.rows+1 || j==mapEditor.cols+1 || mapEditor.obstacles[i][j]==true)
			{
				woodNumber++;
			}
			if(mapEditor.obstacles[i][j]==false)
			{
					guardNumber++;
			}
		}
	}
	complexity = Math.round(100*(woodNumber-4*mapEditor.rows-4)/mapEditor.rows**2);
}

// called when loading the page
$(function () {
	ws = new WebSocket("ws://localhost:8080/test");
  
	ws.onopen = function () {
	  // Web Socket is connected, send data using send()
	  console.log("Ready...");
	};
  
	ws.onmessage = function (evt) {
	  var received_msg = evt.data;
	  console.log("Message is received..." + received_msg);
	};
  
	ws.onclose = function () {
	  // websocket is closed.
	  console.log("Connection is closed...");
	};
});