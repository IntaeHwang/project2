class Guard extends Sprite
{
    private float vX;
	private float vY;
    private int reloadTimeLeft;
    private boolean meAlert;
    private boolean meShoot;
    private boolean meAlive;
    private int firstSight;

    Guard(float x, float y, float r, PImage img, float w)
    {
        super(x,y,r,img,w);
        vX = GUARD_WALK_SPEED;
        vY = GUARD_WALK_SPEED;
        reloadTimeLeft = GUARD_RELOAD_TIME;
        meAlert = false;
        meShoot = false;
        meAlive = true;
        firstSight = 0;
    }
    
    void drawGuard()
    {
        //Direction Constants
        float DEFAULT_ROTATION = PI*3/4;
	    float RIGHT = 0+DEFAULT_ROTATION;
	    float LEFT = PI+DEFAULT_ROTATION;
	    float UP = 3*PI/2+DEFAULT_ROTATION;
	    float DOWN = PI/2+DEFAULT_ROTATION;

        boolean allAlert = observer.isAllAlert();
        float assassinX = observer.assassinX();
        float assassinY = observer.assassinY();
        float d = dist(x,y,assassinX,assassinY);
        int assassinLives = observer.assassinLives();
        boolean gameOver = observer.isGameOver();
        
		if(d<w && assassinLives>0) //Killed
		{
            int guardNumber = observer.guardNumber();
			knife.play();
			guardNumber--;
            observer.updateGuardNumber(guardNumber);
			meAlive = false;
			allAlert = true;
            observer.updateAllAlert(allAlert);
			int alertTime = GUARD_RUN_TIME;
			observer.updateAlertTime(alertTime);
			observer.updateKillSignTime(KILL_SIGN_TIME);
            radio.play();
		}

        //Adjusting vX, vY
		if(allAlert == false && touchingMap(x,y)) //Bouncing off Walls while Walking
		{
			if(r == RIGHT)
			{
				vX = -vX;
				r = LEFT;
			}
			else if(r == LEFT)
			{
				vX = -vX;
				r = RIGHT;
			}
			else if(r == UP)
			{
				vY = -vY;
				r = DOWN;
			}
			else if(r == DOWN)
			{
				vY = -vY;
				r = UP;
			}
		}
		else if(allAlert == true && touchingMap(x,y)) //Bouncing on Walls while Running
		{
			if(r == RIGHT || r == LEFT) vX = -vX;
			else vY = -vY;
		}
		else if(allAlert == true && meAlert == false && !gameOver) //Chasing
		{
			if(abs(assassinX-x) > abs(assassinY-y))
			{
				if(assassinX>x)
				{
					r = RIGHT;
					vX = GUARD_RUN_SPEED;
				}
				else
				{
					r = LEFT;
					vX = -GUARD_RUN_SPEED;
				}
			}
			else
			{
				if(assassinY>y)
				{
					r = DOWN;
					vY = GUARD_RUN_SPEED;
				}
				else
				{
					r = UP;
					vY = -GUARD_RUN_SPEED;
				}
			}
		}
		else if(allAlert == false && meAlert == false || gameOver) //Walking Around
		{
			//Speed Initialize
			if(r==RIGHT) vX = GUARD_WALK_SPEED;
			else if(r==LEFT) vX = -GUARD_WALK_SPEED;
			else if(r==UP) vY = -GUARD_WALK_SPEED;
			else vY = GUARD_WALK_SPEED;

			//Random Movement________________________
			if(int(random(0,100))==0)
			{
				if(r==RIGHT || r==LEFT)
				{

					if(round(random(0,1))==0)
					{
						r = UP;
						if(vY>0) vY = -vY; 
					}
					else
					{
						r = DOWN;
						if(vY<0) vY = -vY; 
					}
				}
				else
				{
					if(round(random(0,1))==0)
					{
						r = RIGHT;
						if(vX<0) vX = -vX;
					}
					else 
					{
						r = LEFT;
						if(vX>0) vX = -vX;
					}
				}
			}
			//______________________________________
		}
		else //Shooting
		{
			vX = 0;
			vY = 0;
			reloadTimeLeft--;
			if(reloadTimeLeft <= 0 && assassinLives>0)
			{
				meShoot = true;
                assassin.shot();
				reloadTimeLeft = GUARD_RELOAD_TIME;
			}
			else meShoot = false;
		}

		//Adding vX, vY
		if(r==RIGHT || r==LEFT) x = x+vX;
		else if(r==UP || r==DOWN) y= y+vY;

        //Drawing Guard
        super.draw();

        //Draw Exclamation Mark
		if(allAlert && meAlert && !gameOver)
		{
			imageMode(CENTER);
			image(exMark, x-(5*15/float(MAP_SIZE)), y-(40*15/float(MAP_SIZE)), exMark.width/4*15/float(MAP_SIZE), exMark.height/4*15/float(MAP_SIZE));
		}

		//Draw Question Mark
		if(allAlert && !meAlert && !gameOver)
		{
			imageMode(CENTER);
			image(qMark, x-(5*15/float(MAP_SIZE)), y-(40*15/float(MAP_SIZE)), qMark.width/4*15/float(MAP_SIZE), qMark.height/4*15/float(MAP_SIZE));
		}
    }

    void drawLight()
    {
        if(meAlert&&firstSight==1)
        {
            surprise.play();
            radio.play();
            firstSight = 2;
        }

        //Light Dimension Variables
		float start = 3*PI/2 - LIGHT_ANGLE/2 - PI/4 + r;
		float end = 3*PI/2 + LIGHT_ANGLE/2 - PI/4 + r;
		float LIGHT_RADIUS = w*LIGHT_RADIUS_RATIO;
		float delta = LIGHT_ANGLE/LIGHT_QUALITY;
		float [] radius = new float [LIGHT_QUALITY];
		
		//Assassin Interaction Variables
		float assassinX = observer.assassinX();
		float assassinY = observer.assassinY();
        int assassinLives = observer.assassinLives();
		float d = dist(x,y,assassinX,assassinY);
		boolean touchingAssassin = false;

		//Finding the radius for each delta arc
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			for(int r=0;r<=LIGHT_RADIUS;r++)
			{
				float p = x-r*cos(start+delta*i-PI);
				float q = y-r*sin(start+delta*i-PI);
				if(touchingMap(p,q))
				{
					radius[i] = r;
					break;
				}
				if(r==LIGHT_RADIUS)
				{
					radius[i] = r;
				}
			}
		}

		//Finding if Light is touching Assassin 
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			PVector v1 = new PVector(cos(start+delta*i) , sin(start+delta*i));
			PVector v2 = new PVector(cos(start+delta*(i+1)+PI/180), sin(start+delta*(i+1)+PI/180));
			PVector v = new PVector(assassinX-x,assassinY-y);
			if(PVector.angleBetween(v1,v)<delta+PI/180 && PVector.angleBetween(v,v2)<delta+PI/180)
			{
				if(d <= radius[i]) touchingAssassin = true;
			}
		}

		//Drawing Light
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			noStroke();
			if(touchingAssassin && assassinLives>0)
			{
				if(meShoot == true)
				{
					fill(255,255,200,255*LIGHT_OPACITY/100);
				}
				else fill(255,0,0,255*LIGHT_OPACITY/100);
				boolean allAlert = true;
                int alertTime = GUARD_RUN_TIME;
                observer.updateAllAlert(allAlert);
                observer.updateAlertTime(alertTime);
				meAlert = true;
                if(firstSight==0) firstSight = 1;
			}
			else
			{
				fill(255,255,255,255*LIGHT_OPACITY/100);
                meAlert = false;
                firstSight = 0;
			}
			arc(x,y,2*radius[i],2*radius[i],start+delta*i,start+delta*(i+1)+PI/180);
		}
    }

    boolean isAlive()
    {
        return meAlive;
    }
}