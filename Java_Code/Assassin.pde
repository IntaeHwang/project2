class Assassin extends Sprite
{
    private int lives;
    private float destinationX, destinationY;
    private float v = ASSASSIN_SPEED;

    Assassin (float x, float y, float r, PImage img, float w)
    {
        super(x,y,r,img,w);
        lives = ASSASSIN_LIVES;
        destinationX = x;
        destinationY = y;
    }

    void drawAssassin()
    {
        PVector direction = new PVector(0,0);
        float prevX = x;
		float prevY = y;
		float vX, vY;
		direction.x = destinationX - x;
		direction.y = destinationY - y;
		if(dist(0,0,direction.x,direction.y)<v)
		{
			x=destinationX;
			y=destinationY;
		}
		else
		{
			if(direction.x>=0) r = PI/2 + atan(direction.y/direction.x);
			if(direction.x<0) r = PI*3/2 + atan(direction.y/direction.x);
			vX = v*direction.x/dist(0,0,direction.x,direction.y);
			vY = v*direction.y/dist(0,0,direction.x,direction.y);
			if(x != destinationX) x = x + vX;
			if(y != destinationY) y = y + vY;
		}
		if(touchingMap(x,y))
		{
			x=prevX;
			y=prevY;
			destinationX = x;
			destinationY = y;
		}
		tint(255,51*lives,51*lives);
        super.draw();
		tint(255);
		observer.updateAssassinCor(x,y);
    }

    //Set Destination to Where Mouse was Clicked
	void setDestination(float x, float y)
	{
		destinationX = x;
		destinationY = y;
	}

	//Assassin Shot
	void shot()
	{
		gunshot.play();
		lives--;
		observer.updateAssassinLives(lives);
	}

	boolean isStill()
	{
		return (x==destinationX && y==destinationY);
	}
}