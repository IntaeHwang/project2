/*
	Project 1
	Name of Project: Hunter Assassin
	Author: Intae Hwang
	Date: 6/25
*/

// your code down here
// feel free to crate other .pde files to organize your code

import processing.sound.*;
import controlP5.*;
import websockets.*;

//Constants
int MAP_SIZE = 15;
int MAP_COMPLEXITY = 20;
int ASSASSIN_LIVES = 5;
int GUARD_NUMBER = 5;
int GUARD_RUN_TIME = 30;
int GUARD_RELOAD_TIME = 8;
int KILL_SIGN_TIME = 10;
//Light
int LIGHT_RADIUS_RATIO = 3; //Recommended Value : 3
int LIGHT_QUALITY = 100; //Recommended Value : 100
float LIGHT_ANGLE = PI/2; //Recommended Value : PI/2
int LIGHT_OPACITY = 50; //Recommended Value : 50
//Dependent Constants
float BLOCK_SIZE;
float ASSASSIN_SPEED;
float GUARD_WALK_SPEED;
float GUARD_RUN_SPEED;

PImage assassinImg;
PImage guardImg;
PImage woodImg;
PImage exMark;
PImage qMark;
PImage victoryText;
PImage defeatText;
PImage restartImg;
PImage skull;

SoundFile bgm;
SoundFile knife;
SoundFile gunshot;
SoundFile radio;
SoundFile surprise;
SoundFile footsteps;
SoundFile victory;
SoundFile defeat;

JSONObject mapInfo;
boolean[][] touchingMap;
Factory factory;
Observer observer;
Assassin assassin;
Guard[] guards;
ControlP5 cp5;
WebsocketServer ws;
boolean customMap = false;

//Runs Once
void setup()
{
	size(900, 900);

	assassinImg = loadImage("assassin.png");
	guardImg = loadImage("guard.png");
	woodImg = loadImage("Wood.png");
	exMark = loadImage ("exmark.png");
	qMark = loadImage ("qmark.png");
	victoryText = loadImage("victory.png");
	defeatText = loadImage("defeat.png");
	restartImg = loadImage("restart.png");
	skull = loadImage("skull.png");

	bgm = new SoundFile (this, "bgm.mp3");
	knife = new SoundFile (this, "knife.mp3");
	gunshot = new SoundFile (this, "gunshot.mp3");
	radio = new SoundFile (this, "radio.mp3");
	surprise = new SoundFile (this, "surprise.mp3");
	footsteps = new SoundFile (this, "footsteps.mp3");
	victory = new SoundFile (this, "victory.mp3");
	defeat = new SoundFile (this, "defeat.mp3");

	cp5 = new ControlP5(this);
	cp5.addSlider("MAP_SIZE").setPosition(75,0).setRange(6,30).setSize(75,16)
	.setColorValue(color(0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));
	cp5.addSlider("MAP_COMPLEXITY").setPosition(75,16).setRange(0, 80).setSize(75,17)
	.setColorValue(color(0,0,0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));
	cp5.addSlider("GUARD_NUMBER").setPosition(75,33).setRange(1, 15).setSize(75,17)
	.setColorValue(color(0,0,0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));
	cp5.addButton("DEF").setValue(0).setPosition(50,0).setSize(25,50)
	.setColorValue(color(255)).setColorActive(color(0))
	.setColorForeground(color(0)).setColorBackground(color(0));

	ws = new WebsocketServer(this, 8080, "/test");
	
	preload();
	bgm.loop();
}

//Runs Repeatedly
void draw()
{
	background(120);
	//Draw Tiles, Assassin, Guards
	drawMap(mapInfo);
	drawGUI();
	timer();
}

//Runs Whenever Mouse is Clicked
void mousePressed()
{
	if(mouseX<=50 && mouseY<=50) preload();
	else if(!(50<mouseX && mouseX<=150 && mouseY<=50)) assassin.setDestination(mouseX, mouseY);
}

void mouseDragged()
{
	if(!(0<mouseX && mouseX<=150 && mouseY<=50)) assassin.setDestination(mouseX, mouseY);
}

//Runs Before Every Round
void preload()
{
	BLOCK_SIZE = 900/(MAP_SIZE+1);
	ASSASSIN_SPEED = 5*15/MAP_SIZE;
	GUARD_WALK_SPEED = 1*15/float(MAP_SIZE);
	GUARD_RUN_SPEED = 4*15/float(MAP_SIZE);
	mapInfo = new JSONObject();
	factory = new Factory();
	observer = new Observer();
	//Create JSONObject
	mapInfo = factory.getRandomMap(MAP_SIZE,MAP_COMPLEXITY,GUARD_NUMBER);
	initTouchingMap();
	//Create Assassin Object
	assassin = factory.getAssassin();
	//Create Guard Object
	guards = factory.getGuards(mapInfo);
}

void preload(JSONObject json)
{
	MAP_SIZE = json.getInt("size");
	MAP_COMPLEXITY = json.getInt("complexity");
	GUARD_NUMBER = json.getInt("guardNumber");
	BLOCK_SIZE = 900/(MAP_SIZE+1);
	ASSASSIN_SPEED = 5*15/MAP_SIZE;
	GUARD_WALK_SPEED = 1*15/float(MAP_SIZE);
	GUARD_RUN_SPEED = 4*15/float(MAP_SIZE);
	mapInfo = new JSONObject();
	factory = new Factory();
	observer = new Observer();
	//Create JSONObject
	mapInfo = json;
	initTouchingMap();
	//Create Assassin Object
	assassin = factory.getAssassin();
	//Create Guard Object
	guards = factory.getGuards(mapInfo);

	cp5.getController("MAP_SIZE").setValue(MAP_SIZE);
	cp5.getController("MAP_COMPLEXITY").setValue(MAP_COMPLEXITY);
	cp5.getController("GUARD_NUMBER").setValue(GUARD_NUMBER);
}

//Runs When Recieved Custom Map
void webSocketServerEvent(String msg)
{
	JSONObject json = parseJSONObject(msg);
	saveJSONObject(json, "data/map.json");
	preload(json);
}

//Draws Map
void drawMap(JSONObject mapInfo)
{
	JSONArray grid = mapInfo.getJSONArray("grid");
	//Draw Tile Shadows
	for(int i=0; i<grid.size(); i++)
	{
		JSONObject block = grid.getJSONObject(i);
		float x = (float) block.getInt("i")*BLOCK_SIZE;
		float y = (float) block.getInt("j")*BLOCK_SIZE;
		String type = block.getString("type");
		if(type.equals("Wood")) factory.getWood(x,y).drawTileShadow();
	}
	//Draw Guards' Light
	for(int i=0; i<guards.length; i++)
	{
		if(guards[i].isAlive())
		{
			guards[i].drawLight();
		}
	}
	//Draw Guards
	for(int i=0; i<guards.length; i++)
	{
		if(guards[i].isAlive())
		{
			guards[i].drawGuard();
		}
	}
	//Draw Assassin
	if(observer.assassinLives()>0) assassin.drawAssassin();
	//Draw Tiles
	for(int i=0; i<grid.size(); i++)
	{
		JSONObject block = grid.getJSONObject(i);
		float x = (float) block.getInt("i")*BLOCK_SIZE;
		float y = (float) block.getInt("j")*BLOCK_SIZE;
		String type = block.getString("type");
		if(type.equals("Wood")) factory.getWood(x,y).drawTile();
	}
}

void drawGUI()
{
	//announcments
	int guardNumber = observer.guardNumber();
	int initialGuardNumber = observer.initialGuardNumber();
	boolean gameOver = observer.isGameOver();
	int assassinLives = observer.assassinLives();
	if(guardNumber==0 && gameOver==false)
	{
		victory.play();
		gameOver = true;
		observer.updateGameOver(gameOver);
	}
	if(assassinLives <= 0 && gameOver==false)
	{
		defeat.play();
		gameOver = true;
		observer.updateGameOver(gameOver);
	}

	//footsteps
	if((assassin.isStill() || assassinLives<=0) && footsteps.isPlaying()==true ) footsteps.pause();
	else if(footsteps.isPlaying()==false && assassinLives>0)
	{
		if(mouseX>150 || mouseY>50) footsteps.loop();
	}

	//bloody screen
	if(!gameOver)
	{
		fill(255,0,0,50*(1-(float(assassinLives)/ASSASSIN_LIVES)));
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);
	}

	//draw kill sign
	int killSignTime = observer.killSignTime();
	if(killSignTime>0)
	{
		imageMode(CENTER);
		tint(255, 175);
		image(skull, 450, 450, skull.width/2, skull.height/2);
		tint(255, 255);
		killSignTime--;
		observer.updateKillSignTime(killSignTime);
	}

	//Game Over Screen
	if(gameOver  && assassinLives>0)
	{
		radio.stop();
		
		fill(255,225,0,100);
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);

		imageMode(CENTER);
		image(victoryText, 450, 450, victoryText.width*1.5, victoryText.height*1.5); 
	}
	else if(gameOver  && assassinLives<=0)
	{
		radio.stop();

		fill(255,0,0,100);
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);

		imageMode(CENTER);
		image(defeatText, 450, 450, defeatText.width*1.5, defeatText.height*1.5); 
	}

	//Draw Restart Button
	imageMode(CORNER);
	image(restartImg, 0, 0, 50, 50);

	//Draw Life bar
	fill(0,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,50,150,25);

	fill(255,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,50,150*assassinLives/ASSASSIN_LIVES,25);

	//Draw Score bar
	fill(0,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,75,150,6);

	fill(0,0,255);
	noStroke();
	rectMode(CORNER);
	rect(0,75,150*(1-(float)guardNumber/initialGuardNumber),6);
}

//Change into Recommended Value
void DEF()
{ 
	cp5.getController("MAP_SIZE").setValue(15);
	cp5.getController("MAP_COMPLEXITY").setValue(20);
	cp5.getController("GUARD_NUMBER").setValue(5);
}

//Checks if an Object is Touching Map
boolean touchingMap(float x, float y)
{
	int i = ceil((x-BLOCK_SIZE/2)/BLOCK_SIZE);
	int j = ceil((y-BLOCK_SIZE/2)/BLOCK_SIZE);
	if(touchingMap[i][j]==true) return true;
	else return false;
}
//Helper Function
void initTouchingMap()
{
	touchingMap = new boolean [MAP_SIZE+2][MAP_SIZE+2];
	JSONArray grid = mapInfo.getJSONArray("grid");
	for(int k=0; k<grid.size(); k++)
	{
		JSONObject block = grid.getJSONObject(k);
		String type = block.getString("type");
		int i = block.getInt("i");
		int j = block.getInt("j");
		if(type.equals("Wood")) touchingMap[i][j]=true;
	}
}

void timer()
{
	//run time measure
	boolean allAlert = observer.isAllAlert();
	int alertTime = observer.alertTime();
	if(allAlert==true)
	{
		alertTime--;
	}
	else alertTime = 0;
	if(alertTime<=0) allAlert=false;
	observer.updateAllAlert(allAlert);
	observer.updateAlertTime(alertTime);
}