abstract class Sprite
{
    protected float x, y;
    protected float w, h;
    protected float r;
    protected PImage img;
    Sprite(float x, float y, float r, PImage img, float w)
    {
        this.x = x;
        this.y = y;
        this.r = r;
        this.w = w;
        this.h = w*float(img.height)/img.width;
        this.img = img;
    }

    void draw()
    {
        imageMode(CENTER);
        translate(x, y);
        rotate(r);
        image(img, 0, 0, w, h);
        rotate(-r);
        translate(-x, -y);
    }

    //abstract void move();
}