import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.sound.*; 
import controlP5.*; 
import websockets.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Java_Code extends PApplet {

/*
	Project 1
	Name of Project: Hunter Assassin
	Author: Intae Hwang
	Date: 6/25
*/

// your code down here
// feel free to crate other .pde files to organize your code





//Constants
int MAP_SIZE = 15;
int MAP_COMPLEXITY = 20;
int ASSASSIN_LIVES = 5;
int GUARD_NUMBER = 5;
int GUARD_RUN_TIME = 30;
int GUARD_RELOAD_TIME = 8;
int KILL_SIGN_TIME = 10;
//Light
int LIGHT_RADIUS_RATIO = 3; //Recommended Value : 3
int LIGHT_QUALITY = 100; //Recommended Value : 100
float LIGHT_ANGLE = PI/2; //Recommended Value : PI/2
int LIGHT_OPACITY = 50; //Recommended Value : 50
//Dependent Constants
float BLOCK_SIZE;
float ASSASSIN_SPEED;
float GUARD_WALK_SPEED;
float GUARD_RUN_SPEED;

PImage assassinImg;
PImage guardImg;
PImage woodImg;
PImage exMark;
PImage qMark;
PImage victoryText;
PImage defeatText;
PImage restartImg;
PImage skull;

SoundFile bgm;
SoundFile knife;
SoundFile gunshot;
SoundFile radio;
SoundFile surprise;
SoundFile footsteps;
SoundFile victory;
SoundFile defeat;

JSONObject mapInfo;
boolean[][] touchingMap;
Factory factory;
Observer observer;
Assassin assassin;
Guard[] guards;
ControlP5 cp5;
WebsocketServer ws;
boolean customMap = false;

//Runs Once
public void setup()
{
	

	assassinImg = loadImage("assassin.png");
	guardImg = loadImage("guard.png");
	woodImg = loadImage("Wood.png");
	exMark = loadImage ("exmark.png");
	qMark = loadImage ("qmark.png");
	victoryText = loadImage("victory.png");
	defeatText = loadImage("defeat.png");
	restartImg = loadImage("restart.png");
	skull = loadImage("skull.png");

	bgm = new SoundFile (this, "bgm.mp3");
	knife = new SoundFile (this, "knife.mp3");
	gunshot = new SoundFile (this, "gunshot.mp3");
	radio = new SoundFile (this, "radio.mp3");
	surprise = new SoundFile (this, "surprise.mp3");
	footsteps = new SoundFile (this, "footsteps.mp3");
	victory = new SoundFile (this, "victory.mp3");
	defeat = new SoundFile (this, "defeat.mp3");

	cp5 = new ControlP5(this);
	cp5.addSlider("MAP_SIZE").setPosition(75,0).setRange(6,30).setSize(75,16)
	.setColorValue(color(0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));
	cp5.addSlider("MAP_COMPLEXITY").setPosition(75,16).setRange(0, 80).setSize(75,17)
	.setColorValue(color(0,0,0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));
	cp5.addSlider("GUARD_NUMBER").setPosition(75,33).setRange(1, 15).setSize(75,17)
	.setColorValue(color(0,0,0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));
	cp5.addButton("DEF").setValue(0).setPosition(50,0).setSize(25,50)
	.setColorValue(color(255)).setColorActive(color(0))
	.setColorForeground(color(0)).setColorBackground(color(0));

	ws = new WebsocketServer(this, 8080, "/test");
	
	preload();
	bgm.loop();
}

//Runs Repeatedly
public void draw()
{
	background(120);
	//Draw Tiles, Assassin, Guards
	drawMap(mapInfo);
	drawGUI();
	timer();
}

//Runs Whenever Mouse is Clicked
public void mousePressed()
{
	if(mouseX<=50 && mouseY<=50) preload();
	else if(!(50<mouseX && mouseX<=150 && mouseY<=50)) assassin.setDestination(mouseX, mouseY);
}

public void mouseDragged()
{
	if(!(0<mouseX && mouseX<=150 && mouseY<=50)) assassin.setDestination(mouseX, mouseY);
}

//Runs Before Every Round
public void preload()
{
	BLOCK_SIZE = 900/(MAP_SIZE+1);
	ASSASSIN_SPEED = 5*15/MAP_SIZE;
	GUARD_WALK_SPEED = 1*15/PApplet.parseFloat(MAP_SIZE);
	GUARD_RUN_SPEED = 4*15/PApplet.parseFloat(MAP_SIZE);
	mapInfo = new JSONObject();
	factory = new Factory();
	observer = new Observer();
	//Create JSONObject
	mapInfo = factory.getRandomMap(MAP_SIZE,MAP_COMPLEXITY,GUARD_NUMBER);
	initTouchingMap();
	//Create Assassin Object
	assassin = factory.getAssassin();
	//Create Guard Object
	guards = factory.getGuards(mapInfo);
}

public void preload(JSONObject json)
{
	MAP_SIZE = json.getInt("size");
	MAP_COMPLEXITY = json.getInt("complexity");
	GUARD_NUMBER = json.getInt("guardNumber");
	BLOCK_SIZE = 900/(MAP_SIZE+1);
	ASSASSIN_SPEED = 5*15/MAP_SIZE;
	GUARD_WALK_SPEED = 1*15/PApplet.parseFloat(MAP_SIZE);
	GUARD_RUN_SPEED = 4*15/PApplet.parseFloat(MAP_SIZE);
	mapInfo = new JSONObject();
	factory = new Factory();
	observer = new Observer();
	//Create JSONObject
	mapInfo = json;
	initTouchingMap();
	//Create Assassin Object
	assassin = factory.getAssassin();
	//Create Guard Object
	guards = factory.getGuards(mapInfo);

	cp5.getController("MAP_SIZE").setValue(MAP_SIZE);
	cp5.getController("MAP_COMPLEXITY").setValue(MAP_COMPLEXITY);
	cp5.getController("GUARD_NUMBER").setValue(GUARD_NUMBER);
}

//Runs When Recieved Custom Map
public void webSocketServerEvent(String msg)
{
	JSONObject json = parseJSONObject(msg);
	saveJSONObject(json, "data/map.json");
	preload(json);
}

//Draws Map
public void drawMap(JSONObject mapInfo)
{
	JSONArray grid = mapInfo.getJSONArray("grid");
	//Draw Tile Shadows
	for(int i=0; i<grid.size(); i++)
	{
		JSONObject block = grid.getJSONObject(i);
		float x = (float) block.getInt("i")*BLOCK_SIZE;
		float y = (float) block.getInt("j")*BLOCK_SIZE;
		String type = block.getString("type");
		if(type.equals("Wood")) factory.getWood(x,y).drawTileShadow();
	}
	//Draw Guards' Light
	for(int i=0; i<guards.length; i++)
	{
		if(guards[i].isAlive())
		{
			guards[i].drawLight();
		}
	}
	//Draw Guards
	for(int i=0; i<guards.length; i++)
	{
		if(guards[i].isAlive())
		{
			guards[i].drawGuard();
		}
	}
	//Draw Assassin
	if(observer.assassinLives()>0) assassin.drawAssassin();
	//Draw Tiles
	for(int i=0; i<grid.size(); i++)
	{
		JSONObject block = grid.getJSONObject(i);
		float x = (float) block.getInt("i")*BLOCK_SIZE;
		float y = (float) block.getInt("j")*BLOCK_SIZE;
		String type = block.getString("type");
		if(type.equals("Wood")) factory.getWood(x,y).drawTile();
	}
}

public void drawGUI()
{
	//announcments
	int guardNumber = observer.guardNumber();
	int initialGuardNumber = observer.initialGuardNumber();
	boolean gameOver = observer.isGameOver();
	int assassinLives = observer.assassinLives();
	if(guardNumber==0 && gameOver==false)
	{
		victory.play();
		gameOver = true;
		observer.updateGameOver(gameOver);
	}
	if(assassinLives <= 0 && gameOver==false)
	{
		defeat.play();
		gameOver = true;
		observer.updateGameOver(gameOver);
	}

	//footsteps
	if((assassin.isStill() || assassinLives<=0) && footsteps.isPlaying()==true ) footsteps.pause();
	else if(footsteps.isPlaying()==false && assassinLives>0)
	{
		if(mouseX>150 || mouseY>50) footsteps.loop();
	}

	//bloody screen
	if(!gameOver)
	{
		fill(255,0,0,50*(1-(PApplet.parseFloat(assassinLives)/ASSASSIN_LIVES)));
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);
	}

	//draw kill sign
	int killSignTime = observer.killSignTime();
	if(killSignTime>0)
	{
		imageMode(CENTER);
		tint(255, 175);
		image(skull, 450, 450, skull.width/2, skull.height/2);
		tint(255, 255);
		killSignTime--;
		observer.updateKillSignTime(killSignTime);
	}

	//Game Over Screen
	if(gameOver  && assassinLives>0)
	{
		radio.stop();
		
		fill(255,225,0,100);
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);

		imageMode(CENTER);
		image(victoryText, 450, 450, victoryText.width*1.5f, victoryText.height*1.5f); 
	}
	else if(gameOver  && assassinLives<=0)
	{
		radio.stop();

		fill(255,0,0,100);
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);

		imageMode(CENTER);
		image(defeatText, 450, 450, defeatText.width*1.5f, defeatText.height*1.5f); 
	}

	//Draw Restart Button
	imageMode(CORNER);
	image(restartImg, 0, 0, 50, 50);

	//Draw Life bar
	fill(0,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,50,150,25);

	fill(255,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,50,150*assassinLives/ASSASSIN_LIVES,25);

	//Draw Score bar
	fill(0,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,75,150,6);

	fill(0,0,255);
	noStroke();
	rectMode(CORNER);
	rect(0,75,150*(1-(float)guardNumber/initialGuardNumber),6);
}

//Change into Recommended Value
public void DEF()
{ 
	cp5.getController("MAP_SIZE").setValue(15);
	cp5.getController("MAP_COMPLEXITY").setValue(20);
	cp5.getController("GUARD_NUMBER").setValue(5);
}

//Checks if an Object is Touching Map
public boolean touchingMap(float x, float y)
{
	int i = ceil((x-BLOCK_SIZE/2)/BLOCK_SIZE);
	int j = ceil((y-BLOCK_SIZE/2)/BLOCK_SIZE);
	if(touchingMap[i][j]==true) return true;
	else return false;
}
//Helper Function
public void initTouchingMap()
{
	touchingMap = new boolean [MAP_SIZE+2][MAP_SIZE+2];
	JSONArray grid = mapInfo.getJSONArray("grid");
	for(int k=0; k<grid.size(); k++)
	{
		JSONObject block = grid.getJSONObject(k);
		String type = block.getString("type");
		int i = block.getInt("i");
		int j = block.getInt("j");
		if(type.equals("Wood")) touchingMap[i][j]=true;
	}
}

public void timer()
{
	//run time measure
	boolean allAlert = observer.isAllAlert();
	int alertTime = observer.alertTime();
	if(allAlert==true)
	{
		alertTime--;
	}
	else alertTime = 0;
	if(alertTime<=0) allAlert=false;
	observer.updateAllAlert(allAlert);
	observer.updateAlertTime(alertTime);
}
class Assassin extends Sprite
{
    private int lives;
    private float destinationX, destinationY;
    private float v = ASSASSIN_SPEED;

    Assassin (float x, float y, float r, PImage img, float w)
    {
        super(x,y,r,img,w);
        lives = ASSASSIN_LIVES;
        destinationX = x;
        destinationY = y;
    }

    public void drawAssassin()
    {
        PVector direction = new PVector(0,0);
        float prevX = x;
		float prevY = y;
		float vX, vY;
		direction.x = destinationX - x;
		direction.y = destinationY - y;
		if(dist(0,0,direction.x,direction.y)<v)
		{
			x=destinationX;
			y=destinationY;
		}
		else
		{
			if(direction.x>=0) r = PI/2 + atan(direction.y/direction.x);
			if(direction.x<0) r = PI*3/2 + atan(direction.y/direction.x);
			vX = v*direction.x/dist(0,0,direction.x,direction.y);
			vY = v*direction.y/dist(0,0,direction.x,direction.y);
			if(x != destinationX) x = x + vX;
			if(y != destinationY) y = y + vY;
		}
		if(touchingMap(x,y))
		{
			x=prevX;
			y=prevY;
			destinationX = x;
			destinationY = y;
		}
		tint(255,51*lives,51*lives);
        super.draw();
		tint(255);
		observer.updateAssassinCor(x,y);
    }

    //Set Destination to Where Mouse was Clicked
	public void setDestination(float x, float y)
	{
		destinationX = x;
		destinationY = y;
	}

	//Assassin Shot
	public void shot()
	{
		gunshot.play();
		lives--;
		observer.updateAssassinLives(lives);
	}

	public boolean isStill()
	{
		return (x==destinationX && y==destinationY);
	}
}
class Factory
{
    public JSONObject getRandomMap (int size, int complexity, int guardNumber)
    {
        JSONObject mapInfo = new JSONObject();
        mapInfo.setInt("size", size);
        mapInfo.setInt("complexity", complexity);
        mapInfo.setInt("guardNumber", guardNumber);
        JSONArray grid = new JSONArray();
        int index = 0;
        int guardCurrNum = 0;
        for(int i=0; i<size+2; i++)
        {
            for(int j=0; j<size+2; j++)
            {
                //Borders Are Always There
                if(i==0 || j==0 || i==size+1 || j==size+1)
                {
                    JSONObject block = new JSONObject();
                    block.setString("type", "Wood");
                    block.setInt("i", i);
                    block.setInt("j", j);
                    grid.setJSONObject(index, block);
                    index++;
                }
                //Exclude Spawn position of Assassin
                //Randomly Create Obstacles
                else if(!(abs((float)(size+1)/2-i)<=0.5f && j==size))
                {
                    if((int)(random(100)+1)<=complexity)
                    {
                        JSONObject block = new JSONObject();
                        block.setString("type", "Wood");
                        block.setInt("i", i);
                        block.setInt("j", j);
                        grid.setJSONObject(index, block);
                        index++;
                    }
                }
            }
        }
        while(guardCurrNum!=guardNumber)
        {
            int i=(int)(random(size)+1);
            int j=(int)(random(size)+1);
            boolean filled = false;
            for(int k=0; k<grid.size(); k++)
            {
                JSONObject block = grid.getJSONObject(k);
		        String type = block.getString("type");
		        if(i==block.getInt("i")&&j==block.getInt("j"))
                {
                    filled = true;
                }
            }
            if(filled==false)
            {
                JSONObject block = new JSONObject();
                block.setString("type", "Guard");
                block.setInt("i", i);
                block.setInt("j", j);
                grid.setJSONObject(index, block);
                index++;
                guardCurrNum++;
            }
        }
        mapInfo.setJSONArray("grid", grid);
        return mapInfo;
    }

    //Create Wood Tile on (x, y)
    public Tile getWood (float x, float y)
    {
        Tile woodTile = new Tile(x,y,0,woodImg,BLOCK_SIZE);
        return woodTile;
    }

    //Create Assassin
    public Assassin getAssassin()
    {
        Assassin assassin = new Assassin(450,900-BLOCK_SIZE,0,assassinImg,BLOCK_SIZE*4/5);
        return assassin;
    }

    //Create Array of Guards
    public Guard[] getGuards(JSONObject mapInfo)
    {
        JSONArray grid = mapInfo.getJSONArray("grid");
        Guard[] guards = new Guard[GUARD_NUMBER];
        int index = 0;
        for(int k=0; k<grid.size(); k++)
	    {
            JSONObject block = grid.getJSONObject(k);
            int i = block.getInt("i");
            int j = block.getInt("j");
		    String type = block.getString("type");
		    if(type.equals("Guard"))
            {
                Guard guard = new Guard(i*BLOCK_SIZE,j*BLOCK_SIZE,PI*3/4,guardImg,BLOCK_SIZE);
                guards[index] = guard;
                index++;
            }
        }
        return guards;
    }
}
class Guard extends Sprite
{
    private float vX;
	private float vY;
    private int reloadTimeLeft;
    private boolean meAlert;
    private boolean meShoot;
    private boolean meAlive;
    private int firstSight;

    Guard(float x, float y, float r, PImage img, float w)
    {
        super(x,y,r,img,w);
        vX = GUARD_WALK_SPEED;
        vY = GUARD_WALK_SPEED;
        reloadTimeLeft = GUARD_RELOAD_TIME;
        meAlert = false;
        meShoot = false;
        meAlive = true;
        firstSight = 0;
    }
    
    public void drawGuard()
    {
        //Direction Constants
        float DEFAULT_ROTATION = PI*3/4;
	    float RIGHT = 0+DEFAULT_ROTATION;
	    float LEFT = PI+DEFAULT_ROTATION;
	    float UP = 3*PI/2+DEFAULT_ROTATION;
	    float DOWN = PI/2+DEFAULT_ROTATION;

        boolean allAlert = observer.isAllAlert();
        float assassinX = observer.assassinX();
        float assassinY = observer.assassinY();
        float d = dist(x,y,assassinX,assassinY);
        int assassinLives = observer.assassinLives();
        boolean gameOver = observer.isGameOver();
        
		if(d<w && assassinLives>0) //Killed
		{
            int guardNumber = observer.guardNumber();
			knife.play();
			guardNumber--;
            observer.updateGuardNumber(guardNumber);
			meAlive = false;
			allAlert = true;
            observer.updateAllAlert(allAlert);
			int alertTime = GUARD_RUN_TIME;
			observer.updateAlertTime(alertTime);
			observer.updateKillSignTime(KILL_SIGN_TIME);
            radio.play();
		}

        //Adjusting vX, vY
		if(allAlert == false && touchingMap(x,y)) //Bouncing off Walls while Walking
		{
			if(r == RIGHT)
			{
				vX = -vX;
				r = LEFT;
			}
			else if(r == LEFT)
			{
				vX = -vX;
				r = RIGHT;
			}
			else if(r == UP)
			{
				vY = -vY;
				r = DOWN;
			}
			else if(r == DOWN)
			{
				vY = -vY;
				r = UP;
			}
		}
		else if(allAlert == true && touchingMap(x,y)) //Bouncing on Walls while Running
		{
			if(r == RIGHT || r == LEFT) vX = -vX;
			else vY = -vY;
		}
		else if(allAlert == true && meAlert == false && !gameOver) //Chasing
		{
			if(abs(assassinX-x) > abs(assassinY-y))
			{
				if(assassinX>x)
				{
					r = RIGHT;
					vX = GUARD_RUN_SPEED;
				}
				else
				{
					r = LEFT;
					vX = -GUARD_RUN_SPEED;
				}
			}
			else
			{
				if(assassinY>y)
				{
					r = DOWN;
					vY = GUARD_RUN_SPEED;
				}
				else
				{
					r = UP;
					vY = -GUARD_RUN_SPEED;
				}
			}
		}
		else if(allAlert == false && meAlert == false || gameOver) //Walking Around
		{
			//Speed Initialize
			if(r==RIGHT) vX = GUARD_WALK_SPEED;
			else if(r==LEFT) vX = -GUARD_WALK_SPEED;
			else if(r==UP) vY = -GUARD_WALK_SPEED;
			else vY = GUARD_WALK_SPEED;

			//Random Movement________________________
			if(PApplet.parseInt(random(0,100))==0)
			{
				if(r==RIGHT || r==LEFT)
				{

					if(round(random(0,1))==0)
					{
						r = UP;
						if(vY>0) vY = -vY; 
					}
					else
					{
						r = DOWN;
						if(vY<0) vY = -vY; 
					}
				}
				else
				{
					if(round(random(0,1))==0)
					{
						r = RIGHT;
						if(vX<0) vX = -vX;
					}
					else 
					{
						r = LEFT;
						if(vX>0) vX = -vX;
					}
				}
			}
			//______________________________________
		}
		else //Shooting
		{
			vX = 0;
			vY = 0;
			reloadTimeLeft--;
			if(reloadTimeLeft <= 0 && assassinLives>0)
			{
				meShoot = true;
                assassin.shot();
				reloadTimeLeft = GUARD_RELOAD_TIME;
			}
			else meShoot = false;
		}

		//Adding vX, vY
		if(r==RIGHT || r==LEFT) x = x+vX;
		else if(r==UP || r==DOWN) y= y+vY;

        //Drawing Guard
        super.draw();

        //Draw Exclamation Mark
		if(allAlert && meAlert && !gameOver)
		{
			imageMode(CENTER);
			image(exMark, x-(5*15/PApplet.parseFloat(MAP_SIZE)), y-(40*15/PApplet.parseFloat(MAP_SIZE)), exMark.width/4*15/PApplet.parseFloat(MAP_SIZE), exMark.height/4*15/PApplet.parseFloat(MAP_SIZE));
		}

		//Draw Question Mark
		if(allAlert && !meAlert && !gameOver)
		{
			imageMode(CENTER);
			image(qMark, x-(5*15/PApplet.parseFloat(MAP_SIZE)), y-(40*15/PApplet.parseFloat(MAP_SIZE)), qMark.width/4*15/PApplet.parseFloat(MAP_SIZE), qMark.height/4*15/PApplet.parseFloat(MAP_SIZE));
		}
    }

    public void drawLight()
    {
        if(meAlert&&firstSight==1)
        {
            surprise.play();
            radio.play();
            firstSight = 2;
        }

        //Light Dimension Variables
		float start = 3*PI/2 - LIGHT_ANGLE/2 - PI/4 + r;
		float end = 3*PI/2 + LIGHT_ANGLE/2 - PI/4 + r;
		float LIGHT_RADIUS = w*LIGHT_RADIUS_RATIO;
		float delta = LIGHT_ANGLE/LIGHT_QUALITY;
		float [] radius = new float [LIGHT_QUALITY];
		
		//Assassin Interaction Variables
		float assassinX = observer.assassinX();
		float assassinY = observer.assassinY();
        int assassinLives = observer.assassinLives();
		float d = dist(x,y,assassinX,assassinY);
		boolean touchingAssassin = false;

		//Finding the radius for each delta arc
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			for(int r=0;r<=LIGHT_RADIUS;r++)
			{
				float p = x-r*cos(start+delta*i-PI);
				float q = y-r*sin(start+delta*i-PI);
				if(touchingMap(p,q))
				{
					radius[i] = r;
					break;
				}
				if(r==LIGHT_RADIUS)
				{
					radius[i] = r;
				}
			}
		}

		//Finding if Light is touching Assassin 
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			PVector v1 = new PVector(cos(start+delta*i) , sin(start+delta*i));
			PVector v2 = new PVector(cos(start+delta*(i+1)+PI/180), sin(start+delta*(i+1)+PI/180));
			PVector v = new PVector(assassinX-x,assassinY-y);
			if(PVector.angleBetween(v1,v)<delta+PI/180 && PVector.angleBetween(v,v2)<delta+PI/180)
			{
				if(d <= radius[i]) touchingAssassin = true;
			}
		}

		//Drawing Light
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			noStroke();
			if(touchingAssassin && assassinLives>0)
			{
				if(meShoot == true)
				{
					fill(255,255,200,255*LIGHT_OPACITY/100);
				}
				else fill(255,0,0,255*LIGHT_OPACITY/100);
				boolean allAlert = true;
                int alertTime = GUARD_RUN_TIME;
                observer.updateAllAlert(allAlert);
                observer.updateAlertTime(alertTime);
				meAlert = true;
                if(firstSight==0) firstSight = 1;
			}
			else
			{
				fill(255,255,255,255*LIGHT_OPACITY/100);
                meAlert = false;
                firstSight = 0;
			}
			arc(x,y,2*radius[i],2*radius[i],start+delta*i,start+delta*(i+1)+PI/180);
		}
    }

    public boolean isAlive()
    {
        return meAlive;
    }
}
class Observer
{
    //Observables
    float assassinX;
    float assassinY;
    int assassinLives=ASSASSIN_LIVES;
    boolean allAlert=false;
    int alertTime=0;
    boolean gameOver=false;
    int guardNumber=GUARD_NUMBER;
    int initialGuardNumber=GUARD_NUMBER;
    int killSignTime=0;
    /////////////////////////////////////////////
    public void updateAssassinCor(float x, float y)
    {
        assassinX = x;
        assassinY = y;
    }

    public void updateAssassinLives(int lives)
    {
        assassinLives = lives;
    }

    public float assassinX()
    {
        return assassinX;
    }

    public float assassinY()
    {
        return assassinY;
    }

    public int assassinLives()
    {
        return assassinLives;
    }
    /////////////////////////////////////////////
    public void updateAllAlert (boolean bool)
    {
        allAlert = bool;
    }

    public void updateAlertTime(int time)
    {
        alertTime = time;
    }

    public boolean isAllAlert()
    {
        return allAlert;
    }

    public int alertTime()
    {
        return alertTime;
    }
    /////////////////////////////////////////////
    public void updateGameOver(boolean bool)
    {
        gameOver = bool;
    }

    public boolean isGameOver()
    {
        return gameOver;
    }
    /////////////////////////////////////////////
    public void updateGuardNumber(int number)
    {
        guardNumber = number;
    }

    public int guardNumber()
    {
        return guardNumber;
    }

    public int initialGuardNumber()
    {
        return initialGuardNumber;
    }
    /////////////////////////////////////////////
    public void updateKillSignTime(int time)
    {
        killSignTime = time;
    }

    public int killSignTime()
    {
        return killSignTime;
    }
}

abstract class Sprite
{
    protected float x, y;
    protected float w, h;
    protected float r;
    protected PImage img;
    Sprite(float x, float y, float r, PImage img, float w)
    {
        this.x = x;
        this.y = y;
        this.r = r;
        this.w = w;
        this.h = w*PApplet.parseFloat(img.height)/img.width;
        this.img = img;
    }

    public void draw()
    {
        imageMode(CENTER);
        translate(x, y);
        rotate(r);
        image(img, 0, 0, w, h);
        rotate(-r);
        translate(-x, -y);
    }

    //abstract void move();
}
class Tile extends Sprite
{
    Tile(float x, float y, float r, PImage img, float w)
    {
        super(x,y,r,img,w);
        h = BLOCK_SIZE;
    }
    
    public void drawTile()
    {
        super.draw();
    }

    public void drawTileShadow()
    {
        fill(0);
		noStroke();
		rectMode(CENTER);
		rect(x,y,w+5,w+5);
    }
}
  public void settings() { 	size(900, 900); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Java_Code" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
