class Observer
{
    //Observables
    float assassinX;
    float assassinY;
    int assassinLives=ASSASSIN_LIVES;
    boolean allAlert=false;
    int alertTime=0;
    boolean gameOver=false;
    int guardNumber=GUARD_NUMBER;
    int initialGuardNumber=GUARD_NUMBER;
    int killSignTime=0;
    /////////////////////////////////////////////
    void updateAssassinCor(float x, float y)
    {
        assassinX = x;
        assassinY = y;
    }

    void updateAssassinLives(int lives)
    {
        assassinLives = lives;
    }

    float assassinX()
    {
        return assassinX;
    }

    float assassinY()
    {
        return assassinY;
    }

    int assassinLives()
    {
        return assassinLives;
    }
    /////////////////////////////////////////////
    void updateAllAlert (boolean bool)
    {
        allAlert = bool;
    }

    void updateAlertTime(int time)
    {
        alertTime = time;
    }

    boolean isAllAlert()
    {
        return allAlert;
    }

    int alertTime()
    {
        return alertTime;
    }
    /////////////////////////////////////////////
    void updateGameOver(boolean bool)
    {
        gameOver = bool;
    }

    boolean isGameOver()
    {
        return gameOver;
    }
    /////////////////////////////////////////////
    void updateGuardNumber(int number)
    {
        guardNumber = number;
    }

    int guardNumber()
    {
        return guardNumber;
    }

    int initialGuardNumber()
    {
        return initialGuardNumber;
    }
    /////////////////////////////////////////////
    void updateKillSignTime(int time)
    {
        killSignTime = time;
    }

    int killSignTime()
    {
        return killSignTime;
    }
}

