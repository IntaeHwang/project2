class Factory
{
    JSONObject getRandomMap (int size, int complexity, int guardNumber)
    {
        JSONObject mapInfo = new JSONObject();
        mapInfo.setInt("size", size);
        mapInfo.setInt("complexity", complexity);
        mapInfo.setInt("guardNumber", guardNumber);
        JSONArray grid = new JSONArray();
        int index = 0;
        int guardCurrNum = 0;
        for(int i=0; i<size+2; i++)
        {
            for(int j=0; j<size+2; j++)
            {
                //Borders Are Always There
                if(i==0 || j==0 || i==size+1 || j==size+1)
                {
                    JSONObject block = new JSONObject();
                    block.setString("type", "Wood");
                    block.setInt("i", i);
                    block.setInt("j", j);
                    grid.setJSONObject(index, block);
                    index++;
                }
                //Exclude Spawn position of Assassin
                //Randomly Create Obstacles
                else if(!(abs((float)(size+1)/2-i)<=0.5 && j==size))
                {
                    if((int)(random(100)+1)<=complexity)
                    {
                        JSONObject block = new JSONObject();
                        block.setString("type", "Wood");
                        block.setInt("i", i);
                        block.setInt("j", j);
                        grid.setJSONObject(index, block);
                        index++;
                    }
                }
            }
        }
        while(guardCurrNum!=guardNumber)
        {
            int i=(int)(random(size)+1);
            int j=(int)(random(size)+1);
            boolean filled = false;
            for(int k=0; k<grid.size(); k++)
            {
                JSONObject block = grid.getJSONObject(k);
		        String type = block.getString("type");
		        if(i==block.getInt("i")&&j==block.getInt("j"))
                {
                    filled = true;
                }
            }
            if(filled==false)
            {
                JSONObject block = new JSONObject();
                block.setString("type", "Guard");
                block.setInt("i", i);
                block.setInt("j", j);
                grid.setJSONObject(index, block);
                index++;
                guardCurrNum++;
            }
        }
        mapInfo.setJSONArray("grid", grid);
        return mapInfo;
    }

    //Create Wood Tile on (x, y)
    Tile getWood (float x, float y)
    {
        Tile woodTile = new Tile(x,y,0,woodImg,BLOCK_SIZE);
        return woodTile;
    }

    //Create Assassin
    Assassin getAssassin()
    {
        Assassin assassin = new Assassin(450,900-BLOCK_SIZE,0,assassinImg,BLOCK_SIZE*4/5);
        return assassin;
    }

    //Create Array of Guards
    Guard[] getGuards(JSONObject mapInfo)
    {
        JSONArray grid = mapInfo.getJSONArray("grid");
        Guard[] guards = new Guard[GUARD_NUMBER];
        int index = 0;
        for(int k=0; k<grid.size(); k++)
	    {
            JSONObject block = grid.getJSONObject(k);
            int i = block.getInt("i");
            int j = block.getInt("j");
		    String type = block.getString("type");
		    if(type.equals("Guard"))
            {
                Guard guard = new Guard(i*BLOCK_SIZE,j*BLOCK_SIZE,PI*3/4,guardImg,BLOCK_SIZE);
                guards[index] = guard;
                index++;
            }
        }
        return guards;
    }
}