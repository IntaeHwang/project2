class Tile extends Sprite
{
    Tile(float x, float y, float r, PImage img, float w)
    {
        super(x,y,r,img,w);
        h = BLOCK_SIZE;
    }
    
    void drawTile()
    {
        super.draw();
    }

    void drawTileShadow()
    {
        fill(0);
		noStroke();
		rectMode(CENTER);
		rect(x,y,w+5,w+5);
    }
}