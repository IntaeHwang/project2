# Hunter Assassin #
* **Name**: Intae Hwang
* **Student ID**: 20180737
* **email**: magicink77@kaist.ac.kr
#
## Table of content

Project 1 (Basically the Same)
* Introduction to Hunter Assassin
* How to play Hunter Assassin
* What's Different about My Hunter Assassin?
* About the Code

Project 2 (New updates, improvements)
* How to Use the Map Customizer
* UPDATED CODE


#
## Introduction to Hunter Assassin

Hunter Assassin is actually an existing mobile game by Ruby Game Studio. Its fun and unique gameplay is what attracted so many users around the world including myself.  

As the title implies, Hunter Assassin lets you play the role as a quiet killer hiding under the shadows avoiding from being seen by any of the armed guards. Your objective is to elliminate all of the scouting guards without getting killed. You'll have to use your surroundings to hide from the flashlights of the guards. But becareful, some actions might alert the guards to come chasing after you.



## How to Play Hunter Assassin

#### Move Assassin
The player becomes the assassin. The assassin is always spawned on the bottem center of the map facing up. The fun part about Hunter Assassin is that the only control you need is the click of a mouse. You can click where you would like to go and the assassin will automatically face and walk towards the destination in a straight line. However when an obstacle blocks the path, you cannot proceed furthur. You will have set another destination just by clicking somewhere else. You can also change your destination even though your not there yet. Basically the assassin will always try to reach the latest point you clicked on.

#### Kill Guard
The assassin uses a knife. Therefore, it can only attack the guards through contact. If you approach a guard close enough, you'll be able to kill it successfuly. When a guard is killed, a red skull icon and some text will apear briefly on the center of your screen.

#### Guards Shoot
Guards are armed with guns. They can shoot you whenever you are inside their range, which is the area that the flashlights reach. When you are shown by the flashlight, the flashlight that found you will turn red, and an exclamation mark will apear above the guard's head. The guard will shoot and reload until you are dead or out of his range. When the assassin is shot, the sprite itself and the whole screen will become bloody.


#### Guards Alert
Guards can communicate through a radio channel. So when a guard sees the assassin or gets killed, all of the other guards will be alert for a certain amount of time. When alert, a question mark will apear above the guard's heads and they will rush towards your position faster than usual. However they also cannot pass through walls nor go around them. They will just stop when confronted by an obstacle.



## What's Different about My Hunter Assassin?

So by now you might be wondering 'Is it any different from the original game?'. On the surface, the characters are identical. The assassin and the guards are directly from the original game. The map textures are different and the additional effects such as killing signs and bloody screens are actually my own idea. (I even think that the rich sound effects and the addition of the announcer's voice made it a little bit fancier than the original.) 

However the real difference is in the rendering of the map. The original game runs in levels where the map is all pre-made stages. I could have just copied some stages and implement it. However I wanted to make a more general platform that can be very user friendlly. So I decided to make a map generater that randomly generates obstacles depending on the user's presets.

On the top left there are everything the user can modify.

#### Restart
The restart button is the big button with a round arrow in it. When pressed, it stops all actions from the previous stage and creates a new game based on the user settings. The restart button is used to modify maps or to start a new game after victory/defeat.

#### Settings
The user settings are on right side of the restart button. There is one default button and three preset sliders. You can modify these three variables using the sliders.
* map size : change map size
* map complexity : change the number of obstacle blocks
* guard number : change the number of guards

The default button is the button half the size of the restart button with a DEF written on it. When pressed, the user presets will be changed into the reccommended values. These are the values I tested and found to be at an appropriate difficulty. I recommend you to play with this setting a bit before trying out the presets.

#### Status Bar
The status bar is actually not something that is adjustable but instead is for showing the user two essential values. That is life, and  progress. The life bar is the thick red one on the top. It obviously represents the life of the assassin. It will decrease when shot and when it reaches 0, the game will end and the defeat banner and voice will apear. On the other hand, the thin blue other one is the progress bar. It starts at 0 and increases as you kill the guards. When all is killed, the victory banner and voice will apear.



## About the Code

In here, I will briefly introduce the ideas and some featurs of the code. For detailed looks, I actually spent a lot of time organizing the code, so I think the code itself will show the flow of work better than words.

#### Libraries
I used two libraries. I used the Sound library to implement the background music and all the other sound effects. Also I used ControlP5 library to make the sliders and buttons for the user presets.

#### Classes (Improved!)
Elaborated on project 2 section.

#### Stuff I'm Proud About
I'm a bit proud of myself on few things, even if it might not be a big deal for anyone else.

Starting from the random movement of guards. I was actually told not to waste a lot of energy on making the bots. However without random movement, the game itself became so loose and boring. Even though it's called hunter assassin, I can proudly say that the guard is the spotlight of the code. The fact that I managed to create a somewhat natural looking movement for the guards really makes me feel good. Also, it does not just randomly move. In case of alert, it chases down the assassin in higher speed and I take pride in that too.

If you look closely at the flashlight, it gets blocked by obstacles so that the assassin could hide in the shadows. This was actually the hardest part to ideate in the whole program. And more importantly, if I weren't able to pull this off, it would be no where near hunter assassin. The thing I did was splitting up the lights into tiny pieces and calculating the value considering the walls. It turned out lookin pretty nice and I had to mention about it here.


#### Bugs & Future Improvements
As this is the first complete version of the game, there are much much more to do. It just has become playable at this moment. There are already some known bugs that I couldn't/didn't fix and some improvements I would like to make.
* flashlight peeking through diagonally meeting walls
* able to kill guard across diagonally meeting walls
* assassin avoiding obstacles to reach destination when mouse clicked
* guard smashing into walls when chasing
* guards only able to move north,west,south,east
* add moving feet
* add shooting fire to guard's gun
* add timer and measure record
* add different background textures

#
## How to Use the Map Customizer
1. Adjust the MAP_SIZE by clicking on the up/down arrows.
2. Go through the inventory and select the block that you need.
3. Select wood and click/drag through empty space to place them.
4. Select guard and click empty space to place them.
5. Click on placed object one more time or use the eraser tool to erase objects.
6. The current MAP_SIZE, MAP_COMPLEXITY, GUARD_NUMBER is updated on the down left display.
7. Click on the big upload button to upload the map into the server.
8. The customized map appears on the processing app.
9. Enjoy!

## UPDATED CODE
#### Previous code
The previous code actually had no significant design patterns. I used classes only for containers and I didn't even split the code into multiple files. I could spend time adding a bunch of features like different textures, skins, etc. However I felt like since this is more about the code i might as well spend some time organizing the one I already had. it actually turned out to be a lot of time so on the surface, there is not much going on after project1. But let me explain my code a bit.
#### Multiple Files
First, I split the file into multiple ones. Since the classes has much changed, I'll show you a list of my files. Java_Code.pde is the main file, and there are classes such as Sprite.pde Assassin.pde Guard.pde Tile.pde Factory.pde and Observable.pde. By spliting them up, I was able to get a good view of my code and make it a more efficient design. 
#### Using JSON instead of 2-dimensional arrays
The feedback was very valuable to me. I figured out that by using JSONObjects and JSONArrays are way more convinient than using multi-dimensional arrays. It's more easier for people to recognize too. I had to turn the array-based functions to read JSONObjects, parse through them and read what ever they need. This was a lot of work than i thought. I literally started a new file and wrote it up from the bottom. As a result I was able to make a clean mapInfo JSONObject which contains the three map settings and the grid JSONArray which contains the wood and the guards.
#### Inheritance
I also felt there was a lot of code being reused so I decided to use inheritence a little bit more than before. It is still simple but it turned out to be okay with me. Starting from the beginning, there is a abstract class called the sprite. This was in project 1 as well, but this time it is a bit more efficient. What I mean by sprite was objects that load images and have to constantly draw them to make them look like they are actually moving or staying in place. The Assassin, the guards and the tiles are the sprites. The Sprite class takes a parameter of x, y, rotation angle, width and PImage file. It calcultes the height and draws the image in the draw() method. This is inherited by the sprites. Tiles are almost the same since it doesn,t move. Assassin and Guards have to move and behave in a specific manner so it turned out to be a bit more complicated. However by using the factory, I was able to make it more simple. All the sprite classes are only responsible of moving their current state and drawing in that state. Since there are only 1 assassin there was no need for a assassin factory, but for the guards and the map I needed to make one.
#### Factory
My Factory class is a little bit different. Usually i believe that a factory returns only one kinf of object. however mine returns a few different objects. First, getWood returns a tile object of wood in a certain position. This tile stays in that position during the draw. Next, getAssassin returns a assassin that spawns in a certain point. This assassin moves around as you know following where you click. This is done by the assassin's draw method. Then, getGuards takes the mapInfo JSON file as a parameter and returns a array of guard objects. By reading the mapInfo, it finds out the spots that are empty and randomly spawn the presetted number of guards. These guards become updated every draw to move randomly, chase assassin and do the stuff they do. Last but not least, getRandomMap takes map size and complexity as a parameter and returns a JSON file containing every information about the map. It randomly creates the terrain based on the complexity and adds it in the JSONArray. With the factory, I was able to put all the complicated random spawn stuff all compactly inside and clean up the rest up the code.
#### Observer
The previous code contained a lot of global variables. I just didn't like it. Since I learned every declaration should be near to the scope, I wanted to reduce the amount of messy global varaibles. So I created a class called the observerthat is supposed to observe every variable that the class need to share with each other. For instance, the life of the assassin should be known to the main function and the guards. So I updated the variable to the observer and the observer would calculate it and show every other class something like isAssassinDead (not exactly) But you get the point. I kinda struggled with how it must update itso frequently, but I think I atleast made something out of it.
#### About the code of Project 2
I spent the majority of the time fixing my processing code and I'm feeling not too bad about it. That doesn't mean I didn't put effort into project 2. The inspiration was obviously from the "Game of Life" homework. So apart from all the websockets and JSOn, the coding structure itself is quite straight forward. But my project 2 was focused on the interface a lot. If I spent a lot of time on the code for project 1, the user interface took me almost that much to complete. I wanted leave only the essentials and make it convinient at the same time. I spent a good amount of time planning out the UI and designing them. And the drawing mechanism is really satisfying. I like that tiny little detail where when you click on a object again while drawing an object it not just erases, but the inventory changes to the eraser tool and when you release the mouse, it get's back to the original tool. This was made by using bruch types as a string and naming them so that with the .include function you can do a couple of tricks.
#### Bottom Line
Overall, I feel satisfied with the result however would have liked to have more additional features added to the original game. I like the way the java code turned out, and how the game itself looks.

#### Honorable Mentions
The GUI is from Minecraft.



## Thank you for reading such a long text!